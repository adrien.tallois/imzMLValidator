/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlvalidator.CVMapping;
import com.alanmrace.jimzmlvalidator.CVMappingHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author alan.race
 */
public class CVMappingFile {
    private final StringProperty filename = new SimpleStringProperty();
    private final StringProperty version  = new SimpleStringProperty();
    private final BooleanProperty include = new SimpleBooleanProperty();
    
    private CVMapping mapping;
    
    public StringProperty filenameProperty() { return filename; }
    public StringProperty versionProperty() { return version; }
    public BooleanProperty includeProperty() { return include; }
    
    public CVMappingFile(String filename) throws FileNotFoundException {
        this.filename.set(filename);
        
        File mappingFile = new File(filename);
        InputStream inputStream = new FileInputStream(mappingFile);
        
        mapping = CVMappingHandler.parseCVMapping(inputStream);
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        version.set(sdf.format(mappingFile.lastModified()));
        //version.set(mapping.getVersion());
        
        include.set(true);
    }
    
    public CVMappingFile(URL url) throws IOException {
        this.filename.setValue(url.getFile());
        
        InputStream inputStream = url.openStream();
        
        mapping = CVMappingHandler.parseCVMapping(inputStream);
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        URLConnection connection = url.openConnection();
        
        version.set(sdf.format(connection.getLastModified()));
        //version.set(mapping.getVersion());
        
        include.set(true);
    }
    
    public CVMapping getCVMapping() {
        return mapping;
    }
}
