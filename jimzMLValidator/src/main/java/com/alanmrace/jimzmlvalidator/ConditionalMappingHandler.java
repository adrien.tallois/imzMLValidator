package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidCVMappingException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author AlanRace
 */
public class ConditionalMappingHandler extends DefaultHandler {
	private static final Logger logger = Logger.getLogger(ConditionalMappingHandler.class.getName());

    protected ConditionalMapping conditionalMapping;

    protected HasMappingRule hasMappingRule;
    protected ConditionalRule currentRule;
    
    protected CVMappingRule currentMappingRule;
    protected CVReferenceList cvReferenceList = new CVReferenceList();

    protected HashMap<String, RuleCollection> ruleCollections = new HashMap<>();
    
    private static final String ATTRIBUTE_EXISTS = "exists";
    private static final String ATTRIBUTE_SCOPEPATH = "scopePath";
    private static final String ATTRIBUTE_TERMACCESSION = "termAccession";
    

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    	try {
	        switch (qName) {
	            case "rules":
	                conditionalMapping = new ConditionalMapping();
	                break;
	            case "ruleCollection":
	                RuleCollection collection = new RuleCollection(attributes.getValue("id"));
	                ruleCollections.put(attributes.getValue("id"), collection);
	                
	                hasMappingRule = collection;
	                break;
	            case "includeRuleCollection":
	                RuleCollection includedCollection = ruleCollections.get(attributes.getValue("id"));
	                
	                for(CVMappingRule rule : includedCollection.getRules()) {
	                    boolean addedRule = false;
	                    
	                    if(currentRule instanceof ConditionalIf) {
	                        if(((ConditionalIf) currentRule).getNumberConditions() > 0) {
	                            Condition condition = ((ConditionalIf) currentRule).getCondition(0);
	
	                            if(condition instanceof DefaultCondition) {
	                                DependentCVMappingRule newRule = new DependentCVMappingRule(((DefaultCondition) condition).getOBOTerm(), 
	                                        rule.getID(), rule.getCVElementPath(), rule.getRequirementLevel(), rule.getScopePath(),
	                                        rule.getCombinationLogic());
	                                
	                                for(CVTerm term : rule.getCVTerms())
	                                    newRule.addCVTerm(term);
	                                
	                                currentRule.addCVMappingRule(newRule);
	                                
	                                addedRule = true;
	                            }
	                        }
	                    }
	                    
	                    if(!addedRule)
	                        currentRule.addCVMappingRule(rule);
	                }
	                
	                break;
	            case "conditions":
	                ConditionalIf.CombinationLogic combinationLogic = ConditionalIf.CombinationLogic.valueOf(attributes.getValue("combinationLogic"));
	                
	                if(combinationLogic != null) {
	                    currentRule = new ConditionalIf(combinationLogic);
	                    ((ConditionalIf)currentRule).setCVReferenceList(cvReferenceList);
	                    
	                    conditionalMapping.addRule(currentRule);
	                    hasMappingRule = currentRule;
	                } else {
	                	logger.log(Level.SEVERE, "Unknown combinationLogic value {0}", attributes.getValue(ATTRIBUTE_EXISTS));
	                }
	                
	                break;
	            case "condition":
	                OBOTerm term = OBO.getOBO().getTerm(attributes.getValue(ATTRIBUTE_TERMACCESSION));
	                String scopePath = attributes.getValue(ATTRIBUTE_SCOPEPATH);
	
	                DefaultCondition.ConditionExistence existence = DefaultCondition.ConditionExistence.valueOf(attributes.getValue(ATTRIBUTE_EXISTS));
	
	                boolean conditionOK = true;
	
	                if (term == null) {
	                	logger.log(Level.SEVERE, "Could not find the term {0}", attributes.getValue(ATTRIBUTE_TERMACCESSION));
	                    conditionOK = false;
	                }
	
	                if (existence == null) {
	                	logger.log(Level.SEVERE, "Unknown exists value {0}", attributes.getValue(ATTRIBUTE_EXISTS));
	                    conditionOK = false;
	                }
	
	                if (conditionOK) {
	                    DefaultCondition condition = new DefaultCondition(existence, scopePath, term);
	                    currentRule.addCondition(condition);
	                }
	
	                break;
	            case "jarCondition":
	                String jarLocation = attributes.getValue("jarLocation");
	                String className = attributes.getValue("className");
	                
	                logger.log(Level.INFO, "Trying to load {0} from {1}", new Object[] {className, jarLocation});
	                
	                currentRule.addCondition(new JarCondition(jarLocation, className));
	                break;
	            case "includeMappingFile": 
	                try {
	                    IncludeMappingFileStatement fileStatement = new IncludeMappingFileStatement(attributes.getValue("filePath"));
	                    
	                    currentRule.addStatement(fileStatement);
	                } catch (FileNotFoundException ex) {
	                    logger.log(Level.SEVERE, null, ex);
	                }
	                
	                break;
	            case "CvReference":
	                cvReferenceList.addCVReference(new CVReference(attributes.getValue("cvName"), attributes.getValue("cvIdentifier")));
	                break;
	            case "CvMappingRule":
	                String requirementLevelText = attributes.getValue("requirementLevel");
	                String combinationLogicText = attributes.getValue("cvTermsCombinationLogic");
	                                
	                boolean generatedRule = false;
	                
	                if(currentRule instanceof ConditionalIf) {
	                    if(((ConditionalIf) currentRule).getNumberConditions() > 0) {
	                        Condition condition = ((ConditionalIf) currentRule).getCondition(0);
	
	                        if(condition instanceof DefaultCondition) {
	                            currentMappingRule = new DependentCVMappingRule(((DefaultCondition) condition).getOBOTerm(), attributes.getValue("id"), attributes.getValue("cvElementPath"),
	                            CVMappingRule.stringToRequirementLevel(requirementLevelText), attributes.getValue(ATTRIBUTE_SCOPEPATH), 
	                            CVMappingRule.stringToCombinationLogic(combinationLogicText));
	                            
	                            generatedRule = true;
	                        }
	                    }
	                }
	                
	                if(!generatedRule) {
	                    currentMappingRule = new CVMappingRule(attributes.getValue("id"), attributes.getValue("cvElementPath"),
	                        CVMappingRule.stringToRequirementLevel(requirementLevelText), attributes.getValue(ATTRIBUTE_SCOPEPATH), 
	                        CVMappingRule.stringToCombinationLogic(combinationLogicText));
	                }
	                
	                // optional: name
	                
	                currentMappingRule.setOBO(OBO.getOBO());
	                hasMappingRule.addCVMappingRule(currentMappingRule);
	                break;
	            case "CvTerm":
	                CVTerm cvTerm = new CVTerm(cvReferenceList.getCVReference(attributes.getValue("cvIdentifierRef")),
	                        attributes.getValue(ATTRIBUTE_TERMACCESSION), 
	                        attributes.getValue("termName"),
	                        CVTerm.stringToBoolean(attributes.getValue("useTerm")), 
	                        CVTerm.stringToBoolean(attributes.getValue("allowChildren"))
	                        );
	                
	                String useTermName = attributes.getValue("useTermName");
	                if(useTermName != null)
	                    cvTerm.setUseTermName(CVTerm.stringToBoolean(useTermName));
	                
	                String isRepeatable = attributes.getValue("isRepeatable");
	                if(isRepeatable != null)
	                    cvTerm.setIsRepeatable(CVTerm.stringToBoolean(isRepeatable)); 
	                
	                currentMappingRule.addCVTerm(cvTerm);
	                
	                break;
	            default:
	            	logger.log(Level.FINE, "Unhandled element {0}", qName);
	            	
	            	break;
	        }
    	} catch (InvalidCVMappingException ex) {
    		logger.log(Level.SEVERE, "Invalid conditional mapping file", ex);
    	}
    }
    public static ConditionalMapping parseConditionalMapping(InputStream conditionalMapping) {
        ConditionalMappingHandler handler = new ConditionalMappingHandler();

        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            //get a new instance of parser
            SAXParser sp = spf.newSAXParser();

            //parse the file and also register this class for call backs
            sp.parse(conditionalMapping, handler);

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return handler.getConditionalMapping();
    }
    
    public ConditionalMapping getConditionalMapping() {
        return conditionalMapping;
    }
}
