/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.MzML;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author alan.race
 */
public class ConditionalMapping {
    List<ConditionalRule> rules;
    
    public ConditionalMapping() {
        rules = new LinkedList<>();
    }
    
    public void addRule(ConditionalRule rule) {
        rules.add(rule);
    }
    
    /**
     * Given a specific (i)mzML file, return the set of CVMapping rules that match 
     * the conditions. 
     * 
     * @param mzML
     * @return
     */
    public List<CVMapping> getMappingList(MzML mzML) {
        List<CVMapping> mappingList = new LinkedList<>();
        
        for(ConditionalRule rule : rules) {
            mappingList.addAll(rule.getMappingList(mzML));
        }
        
        return mappingList;
    }
    
    public List<CVMapping> getFullMappingList() {
        List<CVMapping> mappingList = new LinkedList<>();
        
        for(ConditionalRule rule : rules) {
            mappingList.addAll(rule.getFullMappingList());
        }
        
        return mappingList;
    }
}
