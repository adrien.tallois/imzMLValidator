package com.alanmrace.jimzmlvalidator.reporting;

import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidCVMappingException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Alan Race
 */
public class ImzMLReportHandler extends DefaultHandler {

    private static final Logger logger = Logger.getLogger(ImzMLReportHandler.class.getName());

    ImzMLReport report;
    protected CVMappingRule currentMappingRule;

    protected OBO obo;

    public void setOBO(OBO obo) {
        this.obo = obo;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        try {
            switch (qName) {
                case "ImzMLReport":
                    report = new ImzMLReport();
                    break;
                case "CvMappingRule":
                    String requirementLevelText = attributes.getValue("requirementLevel");
                    String combinationLogicText = attributes.getValue("cvTermsCombinationLogic");

                    currentMappingRule = new CVMappingRule(attributes.getValue("id"), attributes.getValue("cvElementPath"),
                            CVMappingRule.stringToRequirementLevel(requirementLevelText), attributes.getValue("scopePath"),
                            CVMappingRule.stringToCombinationLogic(combinationLogicText));

                    // optional: name
                    currentMappingRule.setOBO(obo);
                    break;
                case "CvTerm":
                    CVTerm cvTerm = new CVTerm(null,
                            attributes.getValue("termAccession"),
                            attributes.getValue("termName"),
                            CVTerm.stringToBoolean(attributes.getValue("useTerm")),
                            CVTerm.stringToBoolean(attributes.getValue("allowChildren"))
                    );
                    String useTermName = attributes.getValue("useTermName");
                    if (useTermName != null) {
                        cvTerm.setUseTermName(CVTerm.stringToBoolean(useTermName));
                    }

                    String isRepeatable = attributes.getValue("isRepeatable");
                    if (isRepeatable != null) {
                        cvTerm.setIsRepeatable(CVTerm.stringToBoolean(isRepeatable));
                    }

                    currentMappingRule.addCVTerm(cvTerm);
                    break;
            }
        } catch (InvalidCVMappingException ex) {
            logger.log(Level.SEVERE, "Invalid conditional mapping file", ex);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "CvMappingRule":
                report.addCVMappingRule(currentMappingRule);
                break;
        }
    }

    public static ImzMLReport parseImzMLReport(InputStream cvMapping) {
        return parseImzMLReport(cvMapping, null);
    }

    public static ImzMLReport parseImzMLReport(InputStream cvMapping, OBO obo) {
        ImzMLReportHandler handler = new ImzMLReportHandler();
        handler.setOBO(obo);

        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            //get a new instance of parser
            SAXParser sp = spf.newSAXParser();

            //parse the file and also register this class for call backs
            sp.parse(cvMapping, handler);

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return handler.getImzMLReport();
    }

    public ImzMLReport getImzMLReport() {
        return report;
    }
}
