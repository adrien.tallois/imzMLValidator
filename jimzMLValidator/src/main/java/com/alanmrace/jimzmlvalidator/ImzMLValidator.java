/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.FatalParseException;
import com.alanmrace.jimzmlparser.exceptions.ImzMLParseException;
import com.alanmrace.jimzmlparser.exceptions.InvalidImzMLIssue;
import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlparser.imzml.ImzML;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.FileContent;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlparser.parser.ImzMLHandler;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidHashException;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidUUIDException;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import com.alanmrace.jimzmlparser.util.UUIDHelper;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidXMLIssue;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.util.logging.Level;

/**
 *
 * @author Alan
 */
public class ImzMLValidator extends MzMLValidator {

    private static final Logger logger = Logger.getLogger(ImzMLValidator.class.getName());

    private boolean performHashCheck = true;

    public enum ValidatorStep {
        VALIDATE_XML,
        VALIDATE_IMZML, // TODO: rethink this name
        VALIDATE_MAPPING,
        CHECK_UUID,
        CHECK_HASH,
        COMPLETE
    }
    
    /**
     * Default constructor. Sets up the default CVMappingList
     */
    public ImzMLValidator() {
        this.listeners = new LinkedList<>();
        
        cvMappingList = ImzMLValidator.getDefaultMappingList();
    }
    
    public static List<CVMapping> getDefaultMappingList() {
        List<CVMapping> cvMappingList = new LinkedList<>();

        // TODO: Test if there exists mapping files within the same directory as the JAR and if so then add them 
        // as mapping files
        
        File jarDirectory = new File(new File(".").getAbsolutePath());
        
        String[] xmlFiles = jarDirectory.list((dir, name) -> name.toLowerCase().endsWith("xml"));
        
        boolean msFileAdded = false;
        boolean msiFileAdded = false;
        boolean sourceFileAdded = false;
        
        if(xmlFiles != null && xmlFiles.length > 0) {
            for(String xmlFile : xmlFiles) {
                try (FileInputStream fileInputStream = new FileInputStream(new File(xmlFile))) {
                    CVMapping mapping = CVMappingHandler.parseCVMapping(fileInputStream, OBO.getOBO());
                    
                    if(mapping != null) {
                        cvMappingList.add(mapping);
                        
                        if(xmlFile.contains("ms-mapping.xml"))
                            msFileAdded = true;
                        if(xmlFile.contains("Ims1.1-mapping.xml"))
                            msiFileAdded = true;
                        if(xmlFile.contains("sourceFile-may-mapping.xml"))
                            sourceFileAdded = true;
                    }
                } catch(Exception ex) {
                    // Skip this file if it fails to be parsed by the handler
                }
            }
        } 
        
        if(!msFileAdded)        
            cvMappingList.add(CVMappingHandler.parseCVMapping(ImzMLValidator.class.getResourceAsStream("/mapping/ms-mapping.xml"), OBO.getOBO()));
        if(!msiFileAdded)
            cvMappingList.add(CVMappingHandler.parseCVMapping(ImzMLValidator.class.getResourceAsStream("/mapping/Ims1.1-mapping.xml"), OBO.getOBO()));

        if(!sourceFileAdded)
            // Additional mapping file to remove errors when sourceFile parameters are used. The mzML validator moved this to an object rule
            cvMappingList.add(CVMappingHandler.parseCVMapping(ImzMLValidator.class.getResourceAsStream("/mapping/sourceFile-may-mapping.xml"), OBO.getOBO()));
        
        logger.log(Level.FINE, "MappingList: {0}", cvMappingList);
        
        return cvMappingList;
    }
    
    @Override
    protected InputStream getXSDStream() {
        return ImzMLValidator.class.getResourceAsStream("/xsd/imzML1.1.1.xsd");
    }

    protected InputStream getAllXSDStream() {
        return ImzMLValidator.class.getResourceAsStream("/xsd/imzML_all1.1.1.xsd");
    }

    public void setPerformHashCheck(boolean performHashCheck) {
        this.performHashCheck = performHashCheck;
    }

    protected boolean validateXMLSchema() {
        logger.log(Level.FINE,"Evaluating XML Schema");

        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        factory.setResourceResolver(new ResourceResolver());

        Source xsdFile = new StreamSource(getXSDStream());
        File xmlFile = new File(filename);

        boolean okToContinue = true;

        ArrayList<Issue> fullIssueList = new ArrayList<>();
        ArrayList<Issue> partialIssueList = new ArrayList<>();

        try {
            Schema schema = factory.newSchema(xsdFile);
            Validator validator = schema.newValidator();

            validator.setErrorHandler(new DefaultHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    logger.log(Level.FINE, "WARNING: {0}", exception.getLocalizedMessage());
                    fullIssueList.add(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    logger.log(Level.FINE, "FATAL: {0}", exception.getLocalizedMessage());
                    fullIssueList.add(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    logger.log(Level.FINE, "ERROR: {0}", exception.getLocalizedMessage());
                    fullIssueList.add(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }
            });

            try (FileReader fileReader = new FileReader(xmlFile)){
                Source source = new SAXSource(new InputSource(fileReader));

                validator.validate(source);
            } catch (IOException ex) {
                Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
                addIssue(new InvalidXMLIssue(ex.getLocalizedMessage(), ex));

                okToContinue = false;
            }

        } catch (org.xml.sax.SAXException ex) {
            Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
        }

        xsdFile = new StreamSource(getAllXSDStream());

        try {
            Schema schema = factory.newSchema(xsdFile);
            Validator validator = schema.newValidator();

            validator.setErrorHandler(new DefaultHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    logger.log(Level.INFO, "WARNING: {0}", exception.getLocalizedMessage());
                    partialIssueList.add(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    logger.log(Level.INFO, "FATAL: {0}", exception.getLocalizedMessage());
                    partialIssueList.add(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    logger.log(Level.INFO, "ERROR: {0}", exception.getLocalizedMessage());
                    partialIssueList.add(new InvalidXMLIssue(exception.getLocalizedMessage(), exception));
                }
            });

            try (FileReader fileReader = new FileReader(xmlFile)){
                Source source = new SAXSource(new InputSource(fileReader));

                validator.validate(source);
            } catch (IOException ex) {
                Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
                addIssue(new InvalidXMLIssue(ex.getLocalizedMessage(), ex));

                okToContinue = false;
            }

        } catch (org.xml.sax.SAXException ex) {
            Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);
        }

        if(partialIssueList.size() != fullIssueList.size()) {
            addIssue(new InvalidImzMLIssue("Invalid ordering of imzML tags", "First error (of " + fullIssueList.size() + "): " + fullIssueList.get(0).getIssueTitle() + " " + fullIssueList.get(0).getIssueMessage()));
        }

        for(Issue issue : partialIssueList) {
            addIssue(issue);
        }

        return okToContinue;
    }

    public void validateUUID() {
        File ibdFile = ((ImzML) mzML).getIBDFile();

        try (
        		FileInputStream fileInputStream = new FileInputStream(ibdFile);
        		InputStream inputStream = new BufferedInputStream(fileInputStream)
        ) {
            byte[] uuid = new byte[16];

            int numBytesRead = inputStream.read(uuid);

            if (numBytesRead != uuid.length) {
                throw new InvalidUUIDException(InvalidUUIDException.Reason.INVALID_UUID, "Number of bytes read for the UUID does not match the expected length of 16: " + numBytesRead);
            }

            CVParam uuidCVParam = ((ImzML) mzML).getFileDescription().getFileContent().getCVParam(FileContent.UUID_IDENTIFICATION_ID);

            if (uuidCVParam == null) {
                addIssue(new InvalidUUIDException(InvalidUUIDException.Reason.NO_UUID_CV_PARAM, "No UUID cvParam found in imzML"));
                return;
            }

            String uuidString = uuidCVParam.getValueAsString().replace("-", "");
            String uuidInIBD = UUIDHelper.byteArrayToUuid(uuid).toString().replace("-", "");

            if (!uuidInIBD.equals(uuidString)) {
                throw new InvalidUUIDException(InvalidUUIDException.Reason.UUID_MISMATCH, "UUID in ibd file (" + uuidInIBD + ") does not match the one stored in imzML file (" + uuidString + ")");
            }
        } catch (IOException ex) {
            logger.log(Level.FINE, "InvalidUUIDException issue", ex);

            addIssue(new InvalidUUIDException(ex.getLocalizedMessage(), ex));
        } catch (InvalidUUIDException ex) {
            logger.log(Level.FINE, "InvalidUUIDException issue", ex);
            
            addIssue(ex);
        }
    }

    public void validateHash() {
        try {
            CVParam hashCVParam = ((ImzML) mzML).getFileDescription().getFileContent().getCVParamOrChild(FileContent.IBD_CHECKSUM_ID);

            if (hashCVParam == null) {
                addIssue(new InvalidHashException(InvalidHashException.Reason.NO_HASH_CV_PARAM, "No ibdChecksum cvParam found in imzML"));
                return;
            }

            String ibdHash = "";

            switch (hashCVParam.getTerm().getID()) {
                case FileContent.SHA1_CHECKSUM_ID:
                    ibdHash = ImzML.calculateSHA1(((ImzML) mzML).getIBDFile().getAbsolutePath());
                    break;
                case FileContent.MD5_CHECKSUM_ID:
                    ibdHash = ImzML.calculateMD5(((ImzML) mzML).getIBDFile().getAbsolutePath());
                    break;
                default:
                    addIssue(new InvalidHashException(InvalidHashException.Reason.UNKNOWN_HASH_ALGORITHM, "Unknown hash algorithm " + hashCVParam.getTerm().getID() + " (" + hashCVParam.getTerm().getName() + ")"));
                    return;
            }

            String imzMLHash = hashCVParam.getValueAsString();

            if (!ibdHash.toLowerCase().equals(imzMLHash.toLowerCase()) || imzMLHash.isEmpty()) {
                addIssue(new InvalidHashException(InvalidHashException.Reason.HASH_MISMATCH, "Checksum in ibd file (" + ibdHash + ") does not match the one stored in imzML file (" + imzMLHash + ")"));
                return;
            }
        } catch (ImzMLParseException ex) {
            addIssue(new InvalidHashException(ex.getLocalizedMessage(), ex));
        }
    }
    
    @Override
    protected boolean validateXML() {
        logger.log(Level.FINE,"Evaluating XML validity");

        try {
            if (mzML == null) {
                mzML = ImzMLHandler.parseimzML(filename, false, this);
            } else {
                // TODO: Rethink this - currently the mzML variable is not updated as
                // this would remove the ability to select the issue and have this
                // select the correct item in the treeItemMap in ImzMLValidatorController 
                ImzMLHandler.parseimzML(filename, false, this);
            }

            obo = mzML.getOBO();
        } catch (FatalParseException ex) {
            Logger.getLogger(MzMLValidator.class.getName()).log(Level.SEVERE, null, ex);

            // Stop here as there's no point continuing if this exception is thrown,
            // as possible causes are:
            //      - SAXException
            //      - FileNotFoundException
            //      - IOException
            //      - ParserConfigurationException
            addIssue(ex.getIssue());

            return false;
        }

        return true;
    }

    @Override
    public boolean validate() {
        super.validate();

        notifyImzMLValidatorListeners(ValidatorStep.CHECK_UUID, true);

        // Check UUID
        validateUUID();

        notifyImzMLValidatorListeners(ValidatorStep.CHECK_UUID, false);

        if (performHashCheck) {
            notifyImzMLValidatorListeners(ValidatorStep.CHECK_HASH, true);

            // Check SHA-1 Hash
            validateHash();

            notifyImzMLValidatorListeners(ValidatorStep.CHECK_HASH, false);
        }

        notifyImzMLValidatorListeners(ValidatorStep.COMPLETE, false);
        
        // If there are no issues, then all is okay
        return !foundIssue;
    }
}
