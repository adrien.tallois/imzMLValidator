/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 *
 * @author alan.race
 */
public class IncludeMappingFileStatement implements Statement {
    protected String filePath;
    protected CVMapping mapping;
    
    public IncludeMappingFileStatement(String filePath) throws FileNotFoundException {
        this.filePath = filePath;
        
        InputStream inputStream = new FileInputStream(new File(filePath));

        this.mapping = CVMappingHandler.parseCVMapping(inputStream);
    }
}
