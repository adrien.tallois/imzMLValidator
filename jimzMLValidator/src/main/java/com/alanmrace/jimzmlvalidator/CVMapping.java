package com.alanmrace.jimzmlvalidator;

/**
 * Representation of a controlled vocabulary mapping file. 
 * 
 * @author Alan Race
 */
public class CVMapping {    
    /**
     * Name of the PSI data exchange schema, e.g. mzML, GelML, MIF.
     */
    protected String modelName;
    
    /**
     * URI of the data exchange schema.
     */
    protected String modelURI;
    
    /**
     * Version number of the model supported by the CvMapping file.
     */
    protected String modelVersion;
    
    /**
     * List of references to external controlled vocabularies referenced by this mapping file.
     */
    protected CVReferenceList cvReferenceList;

    /**
     * List of controlled vocabulary mapping rules.
     */
    protected CVMappingRuleList cvMappingRuleList;
    
    /**
     * Create a representation of a controlled vocabulary mapping file based on the specified model.
     * 
     * @param modelName Name of the PSI data exchange schema, e.g. mzML, GelML, MIF.
     * @param modelURI URI of the data exchange schema.
     * @param modelVersion Version number of the model supported by the CvMapping file.
     */
    public CVMapping(String modelName, String modelURI, String modelVersion) {
        this();
        
        this.modelName = modelName;
        this.modelURI = modelURI;
        this.modelVersion = modelVersion;        
    }
    
    /**
     * Create empty lists.
     */
    protected CVMapping() {
        this.cvReferenceList = new CVReferenceList();
        this.cvMappingRuleList = new CVMappingRuleList();
    }
    
    /**
     * Create shallow copy of the specified CVMapping instance.
     * 
     * @param oldMap CVMapping to copy.
     */
    public CVMapping(CVMapping oldMap) {
        this(oldMap.modelName, oldMap.modelURI, oldMap.modelVersion);
        
        for(CVReference ref : oldMap.cvReferenceList) {
            cvReferenceList.addCVReference(new CVReference(ref));
        }
        
        for(CVMappingRule rule : oldMap.cvMappingRuleList) {
            cvMappingRuleList.addCVMappingRule(rule);
        }
    }
    
    /**
     * Add a controlled vocabulary reference.
     * 
     * @param cvReference Reference to add.
     */
    public void addCVReference(CVReference cvReference) {
        this.cvReferenceList.addCVReference(cvReference);
    }
    
    /**
     * Returns controlled vocabulary reference with specified ID or null if none found.
     * 
     * @param id ID of the reference
     * @return CVReference if ID found, null if not.
     */
    public CVReference getCVReference(String id) {
        return cvReferenceList.getCVReference(id);
    } 
    
    /**
     * Add a controlled vocabulary mapping rule.
     * 
     * @param cvMappingRule Rule to add.
     */
    public void addCVMappingRule(CVMappingRule cvMappingRule) {
        this.cvMappingRuleList.addCVMappingRule(cvMappingRule);
    }
 
    /**
     * Get the full list of mapping rules associated with this mapping instance.
     * 
     * @return Controlled vocabulary mapping rule list.
     */
    public CVMappingRuleList getCVMappingRuleList() {
        return cvMappingRuleList;
    }
    
    /**
     * Set the controlled vocabulary reference list to a new list.
     *  
     * @param referenceList Controlled vocabulary reference list.
     */
    protected void setCVReferenceList(CVReferenceList referenceList) {
        this.cvReferenceList = referenceList;
    }
}
