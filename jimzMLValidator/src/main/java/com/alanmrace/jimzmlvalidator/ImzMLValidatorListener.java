/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.Issue;

/**
 *
 * @author Alan
 */
public interface ImzMLValidatorListener {
    public void startingStep(ImzMLValidator.ValidatorStep step);
    public void finishingStep(ImzMLValidator.ValidatorStep step);
    
    public void issueFound(Issue issue);
}
