package com.alanmrace.jimzmlvalidator.exceptions;

/**
 *
 * @author Alan Race
 */
public class InvalidUUIDException extends InvalidXMLIssue {

    /**
     *
     */
    private static final long serialVersionUID = -5383686950091126195L;

    public enum Reason {
        NO_UUID_CV_PARAM,
        UUID_MISMATCH,
        INVALID_UUID,
        UNKNOWN
    }

    private final Reason reason;

    public InvalidUUIDException(String issueMessage, Exception ex) {
        super(issueMessage, ex);
        
        reason = Reason.UNKNOWN;
    }
    
    public InvalidUUIDException(Reason reason, String message) {
        super(message, null);
        
        this.reason = reason;
    }
    
    @Override
    public String getIssueMessage() {
        if(reason == null)
            return super.getIssueMessage();
        
        return getLocalizedMessage();
    }
    
    @Override
    public String getIssueTitle() {
        if(reason == null)
            return super.getIssueTitle();
        
        return "Invalid UUID";
    }
}
