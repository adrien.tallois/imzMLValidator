package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import java.util.List;

/**
 *
 * @author Alan Race
 */
public class UnexpectedCVParamMappingRuleException extends CVMappingRuleWithMzMLContentException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3489341049689618028L;
	private final List<CVParam> unexpected;
    
    public UnexpectedCVParamMappingRuleException(CVMappingRule mappingRule, MzMLContentWithParams content, List<CVParam> unexpected) {
        super(mappingRule, content);
        
        this.unexpected = unexpected;
    }

    @Override
    public String getIssueTitle() {
        return "Unexpected CVParam(s) found in " + mappingRule.getScopePath();
    }

    @Override
    public String getIssueMessage() {
        boolean first = true;
        
        StringBuilder message = new StringBuilder();
        
        for(CVParam cvParam : unexpected) {
            if(first)
                first = false;
            else
                message.append("\n");
            
            message.append(cvParam);
        }
        
        return message.toString();
    }
}
