package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.ChildUsedMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidCVMappingException;
import com.alanmrace.jimzmlvalidator.exceptions.RepeatedMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.UsedTermMappingRuleException;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Alan Race
 */
public class CVTerm implements Serializable {

    protected String termAccession;
    protected boolean useTermName;
    protected boolean useTerm;
    protected String termName;
    protected boolean isRepeatable;
    protected boolean allowChildren;
    protected CVReference cvReference;

    protected OBOTerm oboTerm;
    
    public CVTerm(CVReference cvReference, String termAccession,
            String termName, boolean useTerm, boolean allowChildren) {
        this.termAccession = termAccession;
        this.useTerm = useTerm;
        this.termName = termName;
        this.allowChildren = allowChildren;
        this.cvReference = cvReference;
    }
    
    public CVTerm(CVTerm oldTerm) {
        this(oldTerm.cvReference, oldTerm.termAccession, oldTerm.termName,
                oldTerm.useTerm, oldTerm.allowChildren);
        
        this.oboTerm = oldTerm.oboTerm;
        this.useTermName = oldTerm.useTermName;
        this.isRepeatable = oldTerm.isRepeatable;
    }
    
    public void setOBOTerm(OBOTerm oboTerm) {
        this.oboTerm = oboTerm;
    }
    
    public OBOTerm getOBOTerm() {
        if(oboTerm == null)
            oboTerm = OBO.getOBO().getTerm(getTermAccession());
        
        return oboTerm;
    }

    public void setUseTermName(boolean useTermName) {
        this.useTermName = useTermName;
    }

    public void setIsRepeatable(boolean isRepeatable) {
        this.isRepeatable = isRepeatable;
    }

    public String getTermAccession() {
        return termAccession;
    }

    public boolean canUseTerm() {
        return useTerm;
    }

    public boolean canUseChildren() {
        return allowChildren;
    }

    public boolean isRepeatable() {
        return isRepeatable;
    }

    public boolean check(MzMLContentWithParams contentToCheck) throws CVMappingRuleException {
        return check(contentToCheck, null);
    }

    public boolean check(MzMLContentWithParams contentToCheck, CVMappingRule parentRule) throws CVMappingRuleException {
        CVParam parent = contentToCheck.getCVParam(getTermAccession());
        List<CVParam> children = contentToCheck.getChildrenOf(getTermAccession(), false);
        
        // Make sure that the parent is not included in the children
        if (parent != null) {
            children.remove(parent);
        }

        if (!canUseTerm() && parent != null) {
            throw new UsedTermMappingRuleException(parentRule, contentToCheck, this);
        }

        if (!canUseChildren() && !children.isEmpty()) {
            throw new ChildUsedMappingRuleException(parentRule, contentToCheck, this);
        }

        if (!isRepeatable() && (children.size() > 1 || (parent != null && !children.isEmpty()))) {
            throw new RepeatedMappingRuleException(parentRule, contentToCheck, this);
        }
        
        return parent != null || !children.isEmpty();
    }

    public boolean check(CVParam paramToCheck) throws CVMappingRuleException {
        return check(paramToCheck, null, null);
    }
    
    public boolean check(CVParam paramToCheck, MzMLContentWithParams contentToCheck, CVMappingRule parentRule) throws CVMappingRuleException {
        boolean isTerm = paramToCheck.getTerm().getID().equals(getTermAccession());
        boolean isChild = paramToCheck.getTerm().hasParent(new OBOTerm(OBO.getOBO(), getTermAccession()));
        
        // Ensure that the term wasn't used if the rule forbids it
        if(!canUseTerm() && isTerm) {
            throw new UsedTermMappingRuleException(parentRule, contentToCheck, this);
        }
        
        if(!canUseChildren() && isChild) {
            throw new ChildUsedMappingRuleException(parentRule, contentToCheck, this);
        }
                
        return isTerm || isChild;
    }
    
    public boolean isOBOTermCovered(OBOTerm term) {
        boolean isTerm = term.getID().equals(getTermAccession());
        boolean isChild = term.hasParent(new OBOTerm(term.getOntology(), getTermAccession()));
        
        // Ensure that the term wasn't used if the rule forbids it
        if(!canUseTerm() && isTerm) {
            return false;
        }
        
        if(!canUseChildren() && isChild) {
            return false;
        }
                
        return isTerm || isChild;
    }
    
    public Set<OBOTerm> getAllowedOBOTerms() {
        Set<OBOTerm> oboList = new LinkedHashSet<>();
        
        OBOTerm parent = OBO.getOBO().getTerm(getTermAccession());

        if(parent != null) {
            if (canUseTerm()) {
                oboList.add(parent);
            }

            if (canUseChildren()) {
                oboList.addAll(parent.getAllChildren(false));
            }
        }
        
        return oboList;
    }
    
    public String getDescription() {
        StringBuilder description = new StringBuilder();

        if (allowChildren) {
            description.append("A child of");
        } else {
            description.append("Exclusively");
        }

        if (useTerm && allowChildren) {
            description.append(" or");
        }

        description.append(" the term");

        description.append(" of ");
        description.append(termAccession);

        if (getOBOTerm() != null) {
            description.append(" (");
            description.append(oboTerm.getName());
            description.append(")");
        }

        if (isRepeatable) {
            description.append(" one or more times");
        } else {
            description.append(" one time only");
        }

        return description.toString();
    }

    public static boolean stringToBoolean(String value) throws InvalidCVMappingException {
        switch (value) {
            case "true":
                return true;
            case "false":
                return false;
            default:
                throw new InvalidCVMappingException("Invalid boolean value " + value);
        }
    }
}
